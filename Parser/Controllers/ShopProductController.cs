﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Parser.Data;

namespace Parser.Controllers
{
    public class ShopProductController : Controller
    {
        private readonly ProductContext _context;

        public ShopProductController(ProductContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Price()
        {
            using var httpClient = new HttpClient()
            {
                Timeout = TimeSpan.FromSeconds(10)
            };

            //var exis = _context.ShopProducts.FirstOrDefault(x => x.Price != 0);
            var exis = await _context.ShopProducts.ToListAsync();
            for (var i = 0; i < exis.Count; i++)
            {
                var response = await httpClient.GetAsync(exis[i].Link);
                var responseData = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                {

                    //< div class="new price" itemprop="price" content="">19 900 руб.</div>
                    var startBlock = "<span class=\"x-premium-product-prices__price \" content=\"";
                    var startPos = responseData.IndexOf(startBlock);
                    var priceAllText = responseData.Substring(startPos + startBlock.Length);

                    var priceText = priceAllText.Substring(0, priceAllText.IndexOf('"'));

                    var price = Convert.ToDouble(priceText);
                    exis[i].Price = price;
                    _context.ShopProducts.Update(exis[i]);
                    _context.SaveChanges();
                    
                }
           }
            return Redirect("/");
        }

        // GET: ShopProduct
        public async Task<IActionResult> Index()
        {
            var productContext = _context.ShopProducts.Include(s => s.ProductProduct).Include(s => s.ProductShop);
            return View(await productContext.ToListAsync());
        }

        // GET: ShopProduct/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.ShopProducts == null)
            {
                return NotFound();
            }

            var shopProduct = await _context.ShopProducts
                .Include(s => s.ProductProduct)
                .Include(s => s.ProductShop)
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (shopProduct == null)
            {
                return NotFound();
            }

            return View(shopProduct);
        }

        // GET: ShopProduct/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.ProductProducts, "ProductId", "Name");
            ViewData["ShopId"] = new SelectList(_context.ProductShops, "ShopId", "Name");
            return View();
        }

        // POST: ShopProduct/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,ShopId,Link,Price")] ShopProduct shopProduct)
        {
            if (ModelState.IsValid)
            {
                _context.Add(shopProduct);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.ProductProducts, "ProductId", "Name", shopProduct.ProductId);
            ViewData["ShopId"] = new SelectList(_context.ProductShops, "ShopId", "Name", shopProduct.ShopId);
            return View(shopProduct);
        }

        // GET: ShopProduct/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.ShopProducts == null)
            {
                return NotFound();
            }

            var shopProduct = await _context.ShopProducts.FindAsync(id);
            if (shopProduct == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.ProductProducts, "ProductId", "ProductId", shopProduct.ProductId);
            ViewData["ShopId"] = new SelectList(_context.ProductShops, "ShopId", "ShopId", shopProduct.ShopId);
            return View(shopProduct);
        }

        // POST: ShopProduct/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductId,ShopId,Link,Price")] ShopProduct shopProduct)
        {
            if (id != shopProduct.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(shopProduct);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ShopProductExists(shopProduct.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.ProductProducts, "ProductId", "ProductId", shopProduct.ProductId);
            ViewData["ShopId"] = new SelectList(_context.ProductShops, "ShopId", "ShopId", shopProduct.ShopId);
            return View(shopProduct);
        }

        // GET: ShopProduct/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.ShopProducts == null)
            {
                return NotFound();
            }

            var shopProduct = await _context.ShopProducts
                .Include(s => s.ProductProduct)
                .Include(s => s.ProductShop)
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (shopProduct == null)
            {
                return NotFound();
            }

            return View(shopProduct);
        }

        // POST: ShopProduct/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.ShopProducts == null)
            {
                return Problem("Entity set 'ProductContext.ShopProducts'  is null.");
            }
            var shopProduct = await _context.ShopProducts.FindAsync(id);
            if (shopProduct != null)
            {
                _context.ShopProducts.Remove(shopProduct);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ShopProductExists(int id)
        {
          return (_context.ShopProducts?.Any(e => e.ProductId == id)).GetValueOrDefault();
        }
    }
}
