﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Parser.Data;

namespace Parser.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductContext _context;

        public ProductController(ProductContext context)
        {
            _context = context;
        }

        // GET: Product
        public async Task<IActionResult> Index()
        {
              return _context.ProductProducts != null ? 
                          View(await _context.ProductProducts.ToListAsync()) :
                          Problem("Entity set 'ProductContext.ProductProducts'  is null.");
        }

        // GET: Product/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.ProductProducts == null)
            {
                return NotFound();
            }

            var productProduct = await _context.ProductProducts
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (productProduct == null)
            {
                return NotFound();
            }

            return View(productProduct);
        }

        // GET: Product/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,Name")] ProductProduct productProduct)
        {
            if (ModelState.IsValid)
            {
                _context.Add(productProduct);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(productProduct);
        }

        // GET: Product/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.ProductProducts == null)
            {
                return NotFound();
            }

            var productProduct = await _context.ProductProducts.FindAsync(id);
            if (productProduct == null)
            {
                return NotFound();
            }
            return View(productProduct);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductId,Name")] ProductProduct productProduct)
        {
            if (id != productProduct.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productProduct);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductProductExists(productProduct.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(productProduct);
        }

        // GET: Product/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.ProductProducts == null)
            {
                return NotFound();
            }

            var productProduct = await _context.ProductProducts
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (productProduct == null)
            {
                return NotFound();
            }

            return View(productProduct);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.ProductProducts == null)
            {
                return Problem("Entity set 'ProductContext.ProductProducts'  is null.");
            }
            var productProduct = await _context.ProductProducts.FindAsync(id);
            if (productProduct != null)
            {
                _context.ProductProducts.Remove(productProduct);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductProductExists(int id)
        {
          return (_context.ProductProducts?.Any(e => e.ProductId == id)).GetValueOrDefault();
        }
    }
}
