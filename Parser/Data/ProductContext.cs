﻿using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace Parser.Data
{
    public class ProductContext : DbContext
    {
        public DbSet<ProductShop> ProductShops { get; set; }
        public DbSet<ProductProduct> ProductProducts { get; set; }

        public DbSet<ShopProduct> ShopProducts { get; set; }

        public DbSet<PriceLog> PriceLogs { get; set; }

        public ProductContext(DbContextOptions<ProductContext> options): base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ShopProduct>().HasKey(u => new { u.ProductId, u.ShopId});
            modelBuilder.Entity<PriceLog>().HasKey(u => new {u.ProductId, u.ShopId, u.Timer});
        }
    }
}
