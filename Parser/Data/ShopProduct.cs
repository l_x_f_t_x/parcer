﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Parser.Data
{
    public class ShopProduct
    {
        [ForeignKey(nameof(ProductProduct))]
        public int ProductId { get; set; }
        public ProductProduct? ProductProduct { get; set; }

        [ForeignKey(nameof(ProductShop))]
        public int ShopId { get; set; }

        public ProductShop? ProductShop { get; set; }
        public string Link { get; set; }

        public double Price { get; set; }
    }
}
