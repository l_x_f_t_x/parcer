﻿using System.ComponentModel.DataAnnotations;

namespace Parser.Data
{
    public class ProductProduct
    {
        [Key]
        public int ProductId { get; set; }
        public string Name { get; set; }
    }
}
