﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Parser.Data;

namespace Parser.Controllers
{
    public class ShopController : Controller
    {
        private readonly ProductContext _context;

        public ShopController(ProductContext context)
        {
            _context = context;
        }

        // GET: Shop
        public async Task<IActionResult> Index()
        {
              return _context.ProductShops != null ? 
                          View(await _context.ProductShops.ToListAsync()) :
                          Problem("Entity set 'ProductContext.ProductShops'  is null.");
        }

        // GET: Shop/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.ProductShops == null)
            {
                return NotFound();
            }

            var productShop = await _context.ProductShops
                .FirstOrDefaultAsync(m => m.ShopId == id);
            if (productShop == null)
            {
                return NotFound();
            }

            return View(productShop);
        }

        // GET: Shop/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Shop/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ShopId,Name")] ProductShop productShop)
        {
            if (ModelState.IsValid)
            {
                _context.Add(productShop);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(productShop);
        }

        // GET: Shop/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.ProductShops == null)
            {
                return NotFound();
            }

            var productShop = await _context.ProductShops.FindAsync(id);
            if (productShop == null)
            {
                return NotFound();
            }
            return View(productShop);
        }

        // POST: Shop/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ShopId,Name")] ProductShop productShop)
        {
            if (id != productShop.ShopId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productShop);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductShopExists(productShop.ShopId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(productShop);
        }

        // GET: Shop/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.ProductShops == null)
            {
                return NotFound();
            }

            var productShop = await _context.ProductShops
                .FirstOrDefaultAsync(m => m.ShopId == id);
            if (productShop == null)
            {
                return NotFound();
            }

            return View(productShop);
        }

        // POST: Shop/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.ProductShops == null)
            {
                return Problem("Entity set 'ProductContext.ProductShops'  is null.");
            }
            var productShop = await _context.ProductShops.FindAsync(id);
            if (productShop != null)
            {
                _context.ProductShops.Remove(productShop);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductShopExists(int id)
        {
          return (_context.ProductShops?.Any(e => e.ShopId == id)).GetValueOrDefault();
        }
    }
}
